<?php

namespace App\Http\Controllers;

use App\Cast;
use DB;
use Illuminate\Http\Request;

class CastsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('list',[
            'title' => 'List of Casts',
            'casts' => DB::table('casts')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('new_cast',[
            'title' => 'List of Casts',
            'subtitle' => 'Add New Cast'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:casts'
        ]);
        $query = DB::table('casts')->insert([
            'name' => $request['name'],
            'age' => $request['age'],
            'biodata' => $request['biodata']
        ]);
        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        return view('detail',[
            'title' => 'List of Casts',
            'subtitle' => 'Cast Detail',
            'casts' =>DB::table('casts')->where('id',$id)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('edit', compact('casts'),[
            'title' => 'List of Casts',
            'subtitle' =>'Edit Cast'
        ]);
    }

    public function update($id, Request $request)
    {
        // dd($id,$request['name'],$request['age'], $request['biodata']);

        $query = DB::table('casts')
            ->where('id', $id)
            ->update([
                'name' => $request['name'],
                'age' => $request['age'],
                'biodata' => $request['biodata']
            ]);

        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        // dd($cast);
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
