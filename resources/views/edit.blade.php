@extends('layout.main')

@section('title', $title)
@section('container')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">{{$subtitle}}</h3>
  </div>
  <div class="card-body col-lg-6">
    <form method="post" action="/edit/{{$casts->id}}">
      @method('PUT')
      @csrf
      
      <div class="input-group">
        <input type="hidden"  value="{{$casts->id}}" name="id" class="form-control">
        <label for="name" class="form-control">Name</label>
        <input type="text" name="name" value="{{$casts->name}}" class="form-control">
      </div>
      <div class="input-group">
        <label for="age" class="form-control">Age</label>
        <input type="text" name="age" value="{{$casts->age}}" class="form-control">
      </div>
      <div class="input-group">
        <label for="biodata" class="form-control">Biodata</label>
        <input type="text" name="biodata" value="{{$casts->biodata}}"" class="form-control">
      </div>
      <button type="submit" class="btn btn-success">Submit</button>
    </form>
  </div>
</div>
@endsection
