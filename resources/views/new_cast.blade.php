@extends('layout.main')

@section('title', $title)
@section('container')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">{{$subtitle}}</h3>
  </div>
  <div class="card-body col-lg-6">
    <form method="post" action="/cast">
      @csrf
      <div class="input-group">
        <label for="name" class="form-control">Name</label>
        <input type="text" name="name" class="form-control">
      </div>
      <div class="input-group">
        <label for="age" class="form-control">Age</label>
        <input type="text" name="age" class="form-control">
      </div>
      <div class="input-group">
        <label for="biodata" class="form-control">Biodata</label>
        <input type="text" name="biodata" class="form-control">
      </div>
      <button type="submit" class="btn btn-success">Submit</button>
    </form>
  </div>
</div>
@endsection
