<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title></title>
</head>
<body>
@extends('layout.main')

@section('title', $title)
@section('container')
<a href="/cast"><<< Back</a>
<div class="card">
  <div class="card-header">
    <h3 class="card-title">{{$subtitle}} - {{$casts[0]->name}}</h3>
  </div>
  <div class="card-body col-lg-6">
    <table>
      <tr>
        <th>Name</th>
        <td>{{$casts[0]->name}}</td>
      </tr>
      <tr>
        <th>Age</th>
        <td>{{$casts[0]->age}}</td>
      </tr>
      <tr>
        <th>Biodata</th>
        <td>{{$casts[0]->biodata}}</td>
      </tr>
    </table>
  </div>
</div>
@endsection
</body>
</html>
