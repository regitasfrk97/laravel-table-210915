@extends('layout.main')

@section('title', $title)

@section('container')
<div class="card">
	<div class="card-header">
		<h3 class="card-title">{{$title}}</h3>
	</div>
	<div class="card-body">
		<a href="{{url('/create')}}" class="btn btn-primary">Add New Cast</a>
		<br>
		<table id="example1" class="table table-bordered table-striped">
          <thead>
	          <tr>
	            <th>#</th>
	            <th>Name</th>
	            <th>Age</th>
	            <th>Biodata</th>
	            <th>Action</th>
	          </tr>
          </thead>
          <tbody>
          	@foreach($casts as $c)
          	<tr>
          		<td>{{$loop->iteration}}</td>
          		<td>{{$c->name}}</td>
          		<td>{{$c->age}}</td>
          		<td>{{$c->biodata}}</td>
          		<td>
          			<a href="/cast/{{$c->id}}" class="btn btn-sm btn-info">Detail</a>
          			<a href="/edit/{{$c->id}}" class="btn btn-sm btn-warning">Edit</a>
          			<form method="post" action="/delete/{{$c->id}}">
          				@method('DELETE')
          				@csrf
          				<input type="submit" class="btn btn-danger btn-sm" value="Delete">
          			</form>
          		</td>
          	</tr>
          	@endforeach
          </tbody>
          <tfoot>
          	<tr>
	            <th>#</th>
	            <th>Name</th>
	            <th>Age</th>
	            <th>Biodata</th>
	            <th>Action</th>
	        </tr>
          </tfoot>
      	</table>
	</div>
</div>
@endsection