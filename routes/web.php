<?php
use App\Http\Controllers\CastsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!


/cast/{cast_id} GET CastController@show menampilkan detail data pemain film dengan id tertentu
/cast/{cast_id}/edit    GET CastController@edit menampilkan form untuk edit pemain film dengan id tertentu
/cast/{cast_id} PUT CastController@update   menyimpan perubahan data pemain film (update) untuk id tertentu
/cast/{cast_id} DELETE  CastController@destroy
|
*/

Route::get('/', function () {
    return view('layout.main',['title' => 'Dashboard']);
});
// '/table' dan '/data-tables
Route::get('/table', function () {
    return view('table',['title' => 'Table']);
});
Route::get('/data-table', function () {
    return view('data', ['title' => 'Data Table']);
});
Route::put('/edit/{cast_id}', 'CastsController@update');
Route::delete('/delete/{cast_id}','CastsController@destroy');
Route::get('/cast', 'CastsController@index');
Route::get('/create', 'CastsController@create');
Route::get('/cast/{cast_id}','CastsController@show');
Route::get('/edit/{cast_id}', 'CastsController@edit');
Route::post('/cast', 'CastsController@store');




